msgid ""
msgstr ""
"PO-Revision-Date: 2021-10-19 22:49+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalit.ie>\n"
"Language-Team: Persian <https://hosted.weblate.org/projects/censorship-no/"
"browser-public-private/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.9-dev\n"

#
#: browser/public-private.md:block 1 (header)
msgid "Using public or private browsing"
msgstr "استفاده از مرورگر عمومی یا خصوصی"

#: browser/public-private.md:block 2 (paragraph)
msgid ""
"As described [in a previous section](../concepts/public-private.md), CENO "
"has two different modes of operation depending on whether you want to share "
"the content that you browse with others (public browsing) or not (private "
"browsing)."
msgstr ""
"همان‌طور که [در بخش پیشین](../concepts/public-private.md) توصیف کردیم، CENO "
"دو حالت یا شیوه‌ی مختلف اجرا دارد، بسته به این‌که آیا می‌خواهید محتوایی را "
"که دارید مرور می‌کنید با دیگران به اشتراک بگذارید (مرور عمومی) یا خیر (مرور "
"خصوصی)."

#: browser/public-private.md:block 3 (paragraph)
msgid ""
"This setting applies *to each tab* that you open in the browser, i.e. you "
"can have public browsing tabs and private browsing tabs. CENO's default "
"whenever you start it or open a new tab (using *New tab* in the app's main "
"menu) is to use public browsing. To open a new tab in private browsing mode, "
"just choose *New private tab* in the main menu."
msgstr ""
"این تنظیم *به هر زبانه*ی گشوده در مرورگرتان قابل اطلاق است، یعنی شما می‌"
"توانید زبانه‌های مرور عمومی و زبانه‌های مرور خصوصی داشته باشید. پیش‌فرض در "
"CENO هرگاه که آن را آغاز می‌کنید یا زبانه‌ای جدید می‌گشایید (با استفاده از "
"*زبانه‌ی جدید* در منوی اصلی نرم‌افزار) استفاده از مرور عمومی برای گشودن یک "
"زبانه‌ی جدید در حالت مرور خصوصی است، فقط *زبانه‌ی خصوص جدید* را در منوی اصلی "
"انتخاب کنید."

#: browser/public-private.md:block 4 (paragraph)
msgid ""
"You can tell public tabs from private ones because public tabs have a "
"lighter (or white) tool bar:"
msgstr ""
"شما می‌توانید زبانه‌های عمومی را از زبانه‌های خصوصی تشخیص دهید چون زبانه‌های "
"عمومی یک نوار ابزار روشن‌تر (یا سفید) دارند:"

#: browser/public-private.md:block 5 (paragraph)
msgid "![Figure: A public browsing tab](images/public-tab.png)"
msgstr "![شکل:‌ یک زبانه‌ی مرور عمومی](images/public-tab.png)"

#: browser/public-private.md:block 6 (paragraph)
msgid "In contrast, private tabs have a darker tool bar:"
msgstr "در مقابل، زبانه‌های خصوصی یک نوار ابزار تیره‌تر دارند:"

#: browser/public-private.md:block 7 (paragraph)
msgid "![Figure: A private browsing tab](images/private-tab.png)"
msgstr "![شکل: یک زبانه‌ی مرور خصوصی](images/private-tab.png)"

#: browser/public-private.md:block 8 (paragraph)
msgid ""
"Once you have loaded a page in a tab, the colored CENO icon in the address "
"bar will help you know how it actually retrieved the different elements of "
"the content. We will cover this icon later on."
msgstr ""
"پس از آن‌که صفحه‌ای را در یک زبانه بارگذاری کردید، علامت رنگی CENO در نوار "
"نشانی به شما کمک خواهد کرد تا بفهمید که چگونه عناصر متفاوت آن محتوا را "
"بازیابی کرده است. ما در ادامه درباره‌ی این علامت توضیح خواهیم داد."
