# Quick start guide

CENO Browser allows you to access any website on the Internet, even if it is censored in your country.  CENO uses a peer-to-peer infrastructure to route your requests, as well as to store and share retrieved content with others.  [Read more about CENO](ceno.md).

## How to get started

You will need an Android device:

1. Install CENO Browser from [Google Play][ceno-gplay], [GitHub][ceno-gh] or [Paskoocheh][ceno-pask].  *No special permissions are needed*.
2. Run it.
3. Browse pages normally to help other users access them; if concerned about privacy for some page or if it is not loading as expected, use a private tab (see [public vs. private browsing](../concepts/public-private.md)).
4. Tap on the CENO notification to stop it completely.

Detailed installation instructions are [here](../browser/install.md).

[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno
[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases
[ceno-pask]: https://paskoocheh.com/tools/124/android.html

## Configuration

CENO Browser should work out-of-the-box.  You can find some [diagnostics and settings](../browser/settings.md) under the *CENO* menu entry.

If you want to make sure that your app is also helping others access blocked content, please [read this section](../browser/bridging.md).

## More questions?

- Please see the [FAQ](faq.md).
- Refer to the [troubleshooting guide](../browser/troubleshooting.md).
- Contact us by writing to <cenoers@equalit.ie>.
